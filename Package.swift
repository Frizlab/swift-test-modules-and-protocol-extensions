// swift-tools-version:4.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription



let package = Package(
	name: "test_modules",
	targets: [
		.target(
			name: "Protocol",
			dependencies: []
		),
		.target(
			name: "Extension",
			dependencies: ["Protocol"]
		),
		.target(
			name: "test_modules",
			dependencies: ["Protocol", "Extension"]
		)
	]
)
