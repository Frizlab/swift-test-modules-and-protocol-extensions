import Protocol



public extension Protocol {
	
	func hello() {
		print("hello! DEFAULT implementation.")
	}
	
}

public func useExtensionInExtension(p: Protocol) {
	p.hello()
}

public func useExtensionInExtensionGeneric<P : Protocol>(p: P) {
	p.hello()
}
