/*
 * Protocol2.swift
 * test_modules
 *
 * Created by François Lamboley on 02/07/2018.
 */

import Foundation



protocol Protocol2 {
}

extension Protocol2 {
	
	func hello2() {
		print("hello2: DEFAULT IMPLEMENTATION")
	}
	
}
